# Presentation
The Presentation is available in ODP format (Open Document Presentation). You can view or edit it by LibreOffice or OpenDocument.

Due to that this presentation will not be presented anywhere else, no merge requests are accepted. Although, if you have any comments, you're more than welcome to post it on Issues.
